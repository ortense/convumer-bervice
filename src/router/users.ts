import { Router } from 'express'
import { listUsers } from '../business/rules/list-users'
import { getUser } from '../business/rules/get-user'

const RESOURCE = '/users'

export const usersRouter = Router()

usersRouter.get(`${RESOURCE}`, (request, response, next) =>
  listUsers()
    .then(users => response.status(200).json(users))
    .catch(next))

usersRouter.get(`${RESOURCE}/:id`, (request, response, next) =>
  getUser(request.params.id)
    .then(user => response.status(200).json(user))
    .catch(next))
