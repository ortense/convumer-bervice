import * as server from './server'

const port = process.env.PORT || 3000

server.start(port)
  .then(() => console.log(`Server up at port ${port}`))
