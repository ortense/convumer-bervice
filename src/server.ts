import express from 'express'
import { router } from './router'
import { errorHandler } from './middlewares/error-handler'

export const app = express()

app.use(express.json())
app.use(router)
app.use(errorHandler)

export function start(port:number|string) {
  return new Promise((resolve) => app.listen(port, resolve))
}
