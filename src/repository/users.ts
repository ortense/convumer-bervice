import axios from 'axios'
import { Repository } from './interfaces'
import { User, Todo } from '../business/interfaces';

export function create():Repository<User> {

  const client = axios.create({ baseURL: 'https://jsonplaceholder.typicode.com' })

  return {
    async getByID(id) {
      const [ user, todos ] = await Promise.all([
        client.get<User>(`/users/${id}`).then(response => response.data),
        client.get<Todo[]>(`/todos?userId=${id}`).then(response => response.data),
      ])
      
      user.todos = todos
      
      return user
    },
    async retrieve() {
      const [ user, todos ] = await Promise.all([
        client.get<User[]>('/users').then(response => response.data),
        client.get<Todo[]>('/todos').then(response => response.data),
      ])

      const todosByUserId = todos.reduce<{ [key:number]:Todo[] }>((accumulator, todo) => {
        accumulator[todo.userId] = accumulator[todo.userId] || []
        accumulator[todo.userId].push(todo)
        return accumulator
      }, {})

      return user.map(user => {
        user.todos = todosByUserId[user.id]
        return user
      })
    }
  }
}
