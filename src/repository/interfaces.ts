export interface Repository<T> {
  getByID(id:string):Promise<T>
  retrieve():Promise<T[]>
}
