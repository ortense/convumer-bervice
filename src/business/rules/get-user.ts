import * as repo from '../../repository/users'
import { ServiceError, ValidationError } from '../errors'

export async function getUser(id:string, repository = repo.create()) {
  if (isNaN(Number(id))) {
    throw new ValidationError('ID de usuário inválido')
  }

  try {
    return await repository.getByID(id)
  }
  catch (error) {
    console.error(error)
    throw new ServiceError(`Não localizar o usuário ${id}.`)
  }
}
