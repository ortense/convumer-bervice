import * as repo from '../../repository/users'
import { ServiceError } from '../errors'

export async function listUsers(repository = repo.create()) {
  try {    
    return await repository.retrieve()
  }
  catch (error) {
    console.error(error)
    throw new ServiceError('Não foi possível listar os usuários')
  }
}
