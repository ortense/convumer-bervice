export class CustomError extends Error {
  code:number
  name:string

  constructor(code:number, name:string, message:string) {
    super(message)
    this.code = code
    this.name = name
  }
}

export class ServiceError extends CustomError {
  constructor(message:string) {
    super(10, 'ServiceError', message)
  }
}

export class ValidationError extends CustomError {
  constructor(message:string) {
    super(20, 'ValidationError', message)
  }
}

